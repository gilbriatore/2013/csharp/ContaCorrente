﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ContaCorrente.Model
{
    class Cliente
    {
        public int Id { set; get; }
        public string Nome { set; get; }
        public string Cpf { set; get; }
    }
}
