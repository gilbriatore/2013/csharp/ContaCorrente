﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ContaCorrente.Model
{
    class Conta
    {
        public int Id { set; get; }
        public Cliente Cliente { set; get; }
        public int Agencia { set; get; }
        public int NumConta { set; get; }
        public DateTime DataAbertura { set; get; }
        public double Limite { set; get; }
        public double Saldo { set; get; }
    }
}
