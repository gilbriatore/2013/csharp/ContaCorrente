﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ContaCorrente.Model;

namespace ContaCorrente.Negocio
{
    class ContaNegocio
    {
        public static Conta Depositar(Conta conta, double valor)
        {
            conta.Saldo += valor;
            return conta;
        }

        public static Conta Sacar(Conta conta, double valor)
        {
            if (conta.Saldo + conta.Limite >= valor)
            {
                conta.Saldo -= valor;
                return conta;
            }
            return null;
        }
    }
}
