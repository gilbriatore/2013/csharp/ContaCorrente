﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ContaCorrente.Model;
using ContaCorrente.DAO;

namespace ContaCorrente.View
{
    public partial class frmLocalizarCliente : Form
    {
        frmCadCli cadCli = null;
        public frmLocalizarCliente()
        {
            InitializeComponent();
        }
        public frmLocalizarCliente(Form form)
        {
            InitializeComponent();
            if (form.Name.Equals("frmCadCli"))
            {
                cadCli = (frmCadCli)form;
            }
        }
        private void pctLocalizar_Click(object sender, EventArgs e)
        {
            long cpf;
            string cpfFormatado;
            Cliente cliente = new Cliente();
            cliente.Nome = txtNome.Text;
            cliente.Cpf = txtNome.Text;
            grdDados.Rows.Clear();
            foreach (Cliente x in ClienteDAO.Search(cliente))
            {
                cpf = Int64.Parse(x.Cpf);
                cpfFormatado = String.Format(@"{0:000\.000\.000\-00}", cpf);
                grdDados.Rows.Add(x.Id, x.Nome, cpfFormatado);
            }
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {

        }

        private void btnCancelar_Click_1(object sender, EventArgs e)
        {
            Close();
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            if (cadCli != null)
            {
                Cliente cliente = new Cliente();
                try
                {
                    cliente.Id = int.Parse(grdDados.CurrentRow.Cells["Id"].Value.ToString());
                    cliente = ClienteDAO.SearchById(cliente);
                    cadCli.txtId.Text = cliente.Id.ToString();
                    cadCli.mskCpf.Text = cliente.Cpf;
                    cadCli.txtNome.Text = cliente.Nome;
                    cadCli.btnEditar.Enabled = true;
                    cadCli.btnExcluir.Enabled = true;
                    Close();
                }
                catch
                {
                    MessageBox.Show("Cliente inválido.", "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        private void txtNome_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar ==(char)13)
            {
                pctLocalizar_Click(sender, e);
            }
        }
          
    }
}
