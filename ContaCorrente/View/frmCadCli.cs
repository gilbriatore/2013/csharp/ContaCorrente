﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ContaCorrente.Util;
using ContaCorrente.Model;
using ContaCorrente.DAO;

namespace ContaCorrente.View
{
    public partial class frmCadCli : Form
    {
        public frmCadCli()
        {
            InitializeComponent();
        }

        private void EnableComponents()
        {
            grpDados.Enabled = true;
            btnOk.Enabled = true;
            btnCancelar.Enabled = true;
            btnNovo.Enabled = false;
            btnEditar.Enabled = false;
            btnExcluir.Enabled = false;
            btnLocalizar.Enabled = false;
        }

        private void DisableComponents()
        {
            grpDados.Enabled = false;
            btnOk.Enabled = false;
            btnCancelar.Enabled = false;
            btnNovo.Enabled = true;
            btnLocalizar.Enabled = true;
        }

        private void btnNovo_Click(object sender, EventArgs e)
        {
            Utilities.ClearFields(this);
            EnableComponents();
            mskCpf.Focus();
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            Utilities.ClearFields(this);
            DisableComponents();
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            Cliente cliente = new Cliente();
            if (txtId.Text == "")
                cliente.Id = 0;
            else
                cliente.Id = int.Parse(txtId.Text);
            cliente.Cpf = mskCpf.Text;
            cliente.Nome = txtNome.Text;
            if (cliente.Id == 0)
            {
                if (!cliente.Cpf.Equals("") && !cliente.Nome.Equals(""))
                {
                    if (Utilities.ValidarCpf(cliente.Cpf))
                    {
                        if (ClienteDAO.SearchByCpf(cliente) == null)
                        {
                            if (ClienteDAO.Insert(cliente))
                            {
                                cliente = ClienteDAO.SearchByCpf(cliente);
                                txtId.Text = cliente.Id.ToString();
                                DisableComponents();
                                btnEditar.Enabled = true;
                                btnExcluir.Enabled = true;
                                MessageBox.Show("Operação bem sucedida.", "Informação", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            }
                            else
                            {
                                MessageBox.Show("A operação não pôde ser realizada.", "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            }
                        }
                        else
                        {
                            MessageBox.Show("Cliente já cadastrado.", "Alerta", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                        }
                    }
                    else
                    {
                        MessageBox.Show("CPF inválido.", "Alerta", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    }
                }
                else
                {
                    MessageBox.Show("Todos os campos são de preenchimento obrigatório.", "Informação", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }
            }
            else
            {
                cliente = ClienteDAO.SearchById(cliente);
                cliente.Nome = txtNome.Text;
                if (!cliente.Nome.Equals(""))
                {
                    if (ClienteDAO.Update(cliente))
                    {
                        DisableComponents();
                        btnEditar.Enabled = true;
                        btnExcluir.Enabled = true;
                        MessageBox.Show("Operação bem sucedida.", "Informação", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    else
                    {
                        MessageBox.Show("A operação não pôde ser realizada.", "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
                else
                {
                    MessageBox.Show("Todos os campos são de preenchimento obrigatório.", "Informação", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }
            }            
        }

        private void btnLocalizar_Click(object sender, EventArgs e)
        {
            frmLocalizarCliente localizar = new frmLocalizarCliente(this);
            localizar.ShowDialog();
        }

        private void btnEditar_Click(object sender, EventArgs e)
        {
            EnableComponents();
            txtNome.Focus();
        }

    }
}
