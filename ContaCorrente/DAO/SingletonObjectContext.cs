﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ContaCorrente.DAO
{
    class SingletonObjectContext
    {
        private static readonly SingletonObjectContext instance = new SingletonObjectContext();
        private readonly BancoEntities context;

        private SingletonObjectContext()
        {
            context = new BancoEntities();
        }

        public static SingletonObjectContext Instance
        {
            get
            {
                return instance;
            }
        }

        public BancoEntities Context
        {
            get
            {
                return context;
            }
        }        
    }
}
