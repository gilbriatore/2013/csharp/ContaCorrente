﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ContaCorrente.Model;
using System.Data;

namespace ContaCorrente.DAO
{
    class ClienteDAO
    {
        public static bool Insert(Cliente cliente)
        {
            BancoEntities db = SingletonObjectContext.Instance.Context;
            try
            {
                db.Clientes.Add(cliente);
                db.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public static List<Cliente> Search(Cliente cliente)
        {
            BancoEntities db = SingletonObjectContext.Instance.Context;
            try
            {
                List<Cliente> clientes = db.Clientes.Where(x => x.Nome.Contains(cliente.Nome) || x.Cpf.Contains(cliente.Cpf)).ToList();
                return clientes;
            }
            catch
            {
                return null;
            }
        }

        public static Cliente SearchById(Cliente cliente)
        {
            BancoEntities db = SingletonObjectContext.Instance.Context;
            try
            {
                cliente = db.Clientes.FirstOrDefault(x => x.Id == cliente.Id);
                return cliente;
            }
            catch
            {
                return null;
            }
        }

        public static Cliente SearchByCpf(Cliente cliente)
        {
            BancoEntities db = SingletonObjectContext.Instance.Context;
            try
            {
                cliente = db.Clientes.FirstOrDefault(x => x.Cpf.Equals(cliente.Cpf));
                return cliente;
            }
            catch
            {
                return null;
            }
        }

        public static bool Update(Cliente cliente)
        {
            BancoEntities db = SingletonObjectContext.Instance.Context;
            try
            {
                db.Entry(cliente).State = EntityState.Modified;
                db.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public static bool Delete(Cliente cliente)
        {
            BancoEntities db = SingletonObjectContext.Instance.Context;
            try
            {
                db.Clientes.Remove(cliente);
                db.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }
    }
}
