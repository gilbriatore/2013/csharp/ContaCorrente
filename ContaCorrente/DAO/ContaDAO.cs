﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ContaCorrente.Model;
using System.Data;

namespace ContaCorrente.DAO
{
    class ContaDAO
    {
        public static bool Insert(Conta conta)
        {
            BancoEntities db = SingletonObjectContext.Instance.Context;
            try
            {
                db.Contas.Add(conta);
                db.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public static bool Update(Conta conta)
        {
            BancoEntities db = SingletonObjectContext.Instance.Context;
            try
            {
                db.Entry(conta).State = EntityState.Modified;
                db.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }


    }
}
